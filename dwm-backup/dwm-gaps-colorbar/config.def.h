/* See LICENSE file for copyright and license details. */


#include <X11/XF86keysym.h>
/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 6;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
// Bar :
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const int user_bh            = 24;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
// Fonts :
static const char *fonts[] = 
{
/* System Fonts : */
"JetBrains Mono:style=Bold:size=10:antialias=true:autohint=true",
//"Fantasque Sans Mono:style=Bold:size=12:antialias=true:autohint=true",
//"monospace:style=Bold:size=10:antialias=true:autohint=true"
/* Emoji & Icon Fonts : */
"JoyPixels:size=10:antialias=true:autohint=true",
"Font Awesome 6 Free Solid:pixelsize=14",
"Font Awesome 6 Brands:pixelsize=14",
/* Nerd Fonts : */
"JetBrainsMono Nerd Font:style=Bold:size=10:antialias=true:autohint=true",
//"FantasqueSansMono Nerd Font:style=Bold:size=12:antialias=true:autohint=true",
//"Roboto Mono for Powerline:pixelsize=14:antialias=true:autohint=true",
//"SauceCodePro Nerd Font Mono:weight=bold:pixelsize=10:antialias=true:hinting=true",
};


/* Theme */
//#include "themes/sweetmars.h"
//#include "themes/hornet.h"
//#include "themes/dark.h"
//#include "themes/darcula.h"
//#include "themes/cherryblossom .h"
//#include "themes/gruvbox.h"
//#include "themes/tomorrow.h"
#include "themes/tomorrow-dark.h"
//#include "themes/tomorrow-night.h"
//#include "themes/tomorrow-magenta.h"
//#include "themes/tomorrow-cyan.h"
//#include "themes/vacuous.h"

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_3, col_1, col_2 },
	[SchemeSel]  = { col_4, col_1, col_4 },
	[SchemeStatus]  = { col_3, col_1, col_1 }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_1, col_4, col_4 }, // Tagbar left selected {text,background,not used but cannot be empty}
    [SchemeTagsNorm]  = { col_3, col_1, col_1 }, // Tagbar left unselected {text,background,not used but cannot be empty}
    [SchemeInfoSel]  = { col_4, col_1, col_4 }, // infobar middle  selected {text,background,not used but cannot be empty}
    [SchemeInfoNorm]  = { col_3, col_1, col_1 }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
//static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
//static const char *tags[] = { "dev", "www", "sys", "doc", "vbox", "chat", "mus", "vid", "gfx" }; 
//static const char *tags[] = {"", "", "", "","", "", "", "", "", "", ""};
//static const char *tags[] = { "I", "II", "III", "IV", "V", "VI" };
//static const char *tags[] = {"", "", "", "", "", "", "", "", ""};
//static const char *tags[] = { "", "", "", "", "", "", "", "", "" };
//static const char *tags[] = {"", "", "", "", "", "", "", "", ""};
//static const char *tags[] = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };
//static const char *tags[] = {"", "", "", "", "", "", "", "", ""};
static const char *tags[] = {"", "", "", "", "", "", "", "", ""};

// Underline Tags :
static const unsigned int ulinepad	= 5;	/* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2;	/* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0;	/* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0;	/* 1 to show underline on all tags, 0 for just the active ones */

// Rules :
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
	
	// Floating
	{ "Steam",    NULL,       NULL,       0,            1,           -1 },
	//{ "obs",      NULL,       NULL,       0,            1,           -1 },
	{ "Virt-manager",      NULL,       NULL,       0,            1,           -1 },
	{ "Nm-connection-editor",      NULL,       NULL,       0,            1,           -1 },
	{ "Pavucontrol",      NULL,       NULL,       0,            1,           -1 },
	{ "Arandr",      NULL,       NULL,       0,            1,           -1 },
	{ "Qalculate-gtk",      NULL,       NULL,       0,            1,           -1 },
	{ "Engrampa",      NULL,       NULL,       0,            1,           -1 },
	//{ "qbittorrent",      NULL,       NULL,       0,            1,           -1 },
	{ "Lxappearance",      NULL,       NULL,       0,            1,           -1},
	{ "Gnome-disks",      NULL,       NULL,       0,            1,           -1 },
	{ "Nitrogen",      NULL,       NULL,       0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "fibonacci.c"
#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	// fibonacci
	{ "[@]",      spiral },
 	{ "[\\]",      dwindle },
 	// grid :
 	{ "HHH",      grid },
	// bstack
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
 	// centermaster : 
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	// cyclelayout :
	{ NULL,       NULL },
};

/* key definitions */
//#define MODKEY Mod1Mask // ALT key
#define MODKEY Mod4Mask		// Win key
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-l", "20", "-p", "Run : ", NULL };
static const char *termcmd[]  = { "st", NULL };
/* Programs */
static const char *alacritty[]  = { "alacritty", NULL };
static const char *terminator[]  = { "terminator", NULL };
static const char *filemgr[]  = { "pcmanfm", NULL };
static const char *roficmd[] = { "rofi", "-show", "run", "-show-icons", NULL };

/*Screen Light */
static const char *brupcmd[] = { "brightnessctl", "set", "5%+", NULL };
static const char *brdowncmd[] = { "brightnessctl", "set", "5%-", NULL };

/* Volume */
static const char *upvol[]   = { "pamixer", "-i", "5", NULL };
static const char *downvol[] = { "pamixer", "-d", "5", NULL };
static const char *mutevol[] = { "pamixer", "-t" };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	//{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_p,      spawn,          {.v = roficmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = alacritty } },
	{ ControlMask,             		XK_t, 	   spawn,          {.v = termcmd } },
	{ ControlMask|ShiftMask,        XK_t, 	   spawn,          {.v = terminator } },
	{ ControlMask|ShiftMask,        XK_f, 	   spawn,          {.v = filemgr } },

    // Light :
	{ 0, XF86XK_MonBrightnessUp,               spawn,          {.v = brupcmd } },
    { 0, XF86XK_MonBrightnessDown,             spawn,          {.v = brdowncmd } },
    
    // Volume :
    { 0, XF86XK_AudioMute, 					   spawn, 		   {.v = mutevol } },
	{ 0, XF86XK_AudioLowerVolume,              spawn,          {.v = downvol } },
	{ 0, XF86XK_AudioRaiseVolume,              spawn,          {.v = upvol } },
	
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	// focusstack :
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	// movestack :
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	
	// Layouts :
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	// fibonacci
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[3]} },	
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[4]} },
	// grid :
	{ MODKEY,                       XK_g,      setlayout,      {.v = &layouts[5]} },
	// bottomstack :
	{ MODKEY,                       XK_u,      setlayout,      {.v = &layouts[6]} },
	{ MODKEY,                       XK_o,      setlayout,      {.v = &layouts[7]} },
	// centeredmaster :
	{ MODKEY,                       XK_n,      setlayout,      {.v = &layouts[8]} },
	{ MODKEY|ShiftMask,             XK_n,      setlayout,      {.v = &layouts[9]} },
	// cyclelayout :
	{ ControlMask,					XK_comma,  cyclelayout,    {.i = -1 } },
	{ ControlMask,           		XK_period, cyclelayout,    {.i = +1 } },
	// setlayout :
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	
	{ MODKEY,                       XK_agrave, view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_agrave, tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_semicolon, focusmon,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_semicolon, tagmon,      {.i = +1 } },
	TAGKEYS(                        XK_ampersand,              0)
	TAGKEYS(                        XK_eacute,                 1)
	TAGKEYS(                        XK_quotedbl,               2)
	TAGKEYS(                        XK_apostrophe,             3)
	TAGKEYS(                        XK_parenleft,              4)
	TAGKEYS(                        XK_minus,                  5)
	TAGKEYS(                        XK_egrave,                 6)
	TAGKEYS(                        XK_underscore,             7)
	TAGKEYS(                        XK_ccedilla,               8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

