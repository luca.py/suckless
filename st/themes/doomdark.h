/* 8 normal colors */
static const char black[]		= "#1c1f24";
static const char red[]			= "#ff6c6b"; 
static const char green[] 		= "#98be65";
static const char yellow[]		= "#da8548";
static const char blue[] 		= "#51afef";
static const char magenta[]		= "#c678dd";
static const char cyan[]  		= "#5699af";
static const char white[] 		= "#202328";

/* 8 bright colors */
static const char b_black[]		= "#5b6268";
static const char b_red[]		= "#da8548"; 
static const char b_green[] 	= "#4db5bd";
static const char b_yellow[]	= "#ecbe7b";
static const char b_blue[] 		= "#3071db";
static const char b_magenta[]	= "#a9a1e1";
static const char b_cyan[]  	= "#46d9ff";
static const char b_white[] 	= "#dfdfdf";

/* special colors */
static const char background[]  = "#1D1F21";
static const char foreground[] 	= "#bbc2cf";
