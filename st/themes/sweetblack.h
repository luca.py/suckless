/* 8 normal colors */
static const char black[]		= "#1a1e21";
static const char red[]			= "#bc2a29"; 
static const char green[] 		= "#81957c";
static const char yellow[]		= "#f9bb80";
static const char blue[] 		= "#2a708d";
static const char magenta[]		= "#d254f9";
static const char cyan[]  		= "#217f8e";
static const char white[] 		= "#909090";

/* 8 bright colors */
static const char b_black[]		= "#606060";
static const char b_red[]		= "#d14548"; 
static const char b_green[] 	= "#a7b79a";
static const char b_yellow[]	= "#fae3a0";
static const char b_blue[] 		= "#7491a1";
static const char b_magenta[]	= "#87314e";
static const char b_cyan[]  	= "#0f829d";
static const char b_white[] 	= "#fff0f0";

/* special colors */
static const char background[]  = "#1a1e21";
static const char foreground[] 	= "#d7d7d7";
